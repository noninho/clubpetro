'use strict'

// Http
const http = require('http');

// App
const app = require('../app');

// Config
const config = require('../src/config');

// Cria o servidor
const server = http.createServer(app);

// Escuta na porta setada no config.js
server.listen(config.SV_PORT);

console.log('Servidor rodando na porta ' + config.SV_PORT);