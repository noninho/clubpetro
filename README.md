# Readme

Projeto desenvolvido em NodeJS para ClubPetro.

## Instruções

Clone do projeto: 
> git clone https://noninho@bitbucket.org/noninho/clubpetro.git

Rodar o npm install para instalar as dependências do projeto:
> npm install

Iniciar o projeto
> npm start