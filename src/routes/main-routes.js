'use strict'

const express = require('express');
const router = express.Router();
const mainController = require('../controllers/mainController');

// Rota principal
const rotaPrincipal = router.get('/', mainController.paginaInicial);

module.exports = router