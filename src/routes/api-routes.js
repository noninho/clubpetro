'use strict'

const express = require('express');
const router = express.Router();
const apiController = require('../controllers/apiController');

// Rota para listagem dos usuários
const listagemUsuarios = router.get('/users', apiController.getUsers);

// Rota para detalhes do usuário
const detalhesUsuario = router.get('/users/:username/details', apiController.getUserDetails);

// Rota para os repositorios do usuario
const repositoriosUsuario = router.get('/users/:username/repos', apiController.getUserRepos);

module.exports = router