'use strict'

const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');

// Rota para todos os usuarios
const getUser = router.get('/', userController.getUsers);

// Rota para detalhes do usuario
const getUserDetails = router.get('/:username/details', userController.getUserDetails);

// Rotas para os repositorios do usuario
const userRepos = router.get('/:username/repos', userController.getUserRepos);

// Get Usuarios
module.exports = router;