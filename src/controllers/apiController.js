'use strict'

const apiRepository = require('../repositories/api-repository');

// Retorna o JSON de todos os usuários
exports.getUsers = async (req, res) => {

    try {
        // verifica a pagina
        var since = req.query.since;

        // Busca os dados pelo repositorio
        var dadosJson = await apiRepository.getUsers(req, since);

        res.status(200).send(JSON.stringify(dadosJson));
    } catch (error) {
        res.status(400).send({
            message: 'Erro ao buscar usuários',
            error: error.message
        });
    }

}

// Retorna o json com os detalhes do usuário
exports.getUserDetails = async (req, res) => {

    try {
        // Busca os detalhes do usuario pelo repository
        let user = await apiRepository.getUserDetails(req.params.username);

        // Envia resposta
        res.status(200).send(JSON.stringify(user));
    } catch (error) {
        res.status(400).send({
            message: 'Erro ao buscar detalhes do usuário',
            error: error.message
        });
    }

}

// Retorna o JSON dos repositorios do usuário
exports.getUserRepos = async (req, res) => {

    try {
        // Busca os repositorios atravéz do repository
        const repositories = await apiRepository.getUserRepos(req.params.username);

        res.status(200).send(JSON.stringify(repositories));
    } catch (error) {
        res.status(400).send({
            message: 'Erro ao buscar repositórios do usuário',
            error: error.message
        });
    }

}