'use strict'

const apiRepository = require('../repositories/api-repository');
const axios = require('axios');
const config = require('../config');

// Retorna todos os usuarios
exports.getUsers = async(req, res) => {
    // since 
    var since = req.query.since;

    // Verifica o since
    if (!since) {
        since = 0;
    }

    // URL
    const URL = config.URL_BASE_API + '/users?since=' + since;

    try {
        // Consome o JSON da api
        const response = await axios.get(URL);

        res.render('users', {users: response.data.users, lastSince: response.data.lastSince});
    } catch (error) {
        res.send(error);
    }
}


// Retorna os detalhes do usuário
exports.getUserDetails = async(req, res) => {
    
    try {
        // URL
        const URL = config.URL_BASE_API + '/users/' + req.params.username + '/details';

        // Consome o JSON da API
        const response = await axios.get(URL);

        res.status(200).render('userDetails', {user: response.data});
    } catch (error) {
        res.status(400).send({
            message: 'Erro ao buscar detalhes do usuário',
            error: error.message
        });
    }
}


// Retorna os repositórios do usuário
exports.getUserRepos = async(req, res) => {
    try {
        // Busca o usuário
        const user = await apiRepository.getUserDetails(req.params.username);

        // URL
        const URL = config.URL_BASE_API + '/users/' + req.params.username + '/repos';

        // Consome o JSON da API
        const response = await axios.get(URL);

        res.status(200).render('userRepositories', {repositories: response.data, user: user});
    } catch (error) {
        res.status(400).send({
            message: 'Erro ao buscar repositorios do usuário',
            error: error.message
        });
    }
}