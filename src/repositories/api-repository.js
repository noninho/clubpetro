'use strict'

const axios = require('axios');

// Retorna o json de todos os usuarios
exports.getUsers = async(req, since) => {

    // Seta o since se não houver
    if (!since) {
        since = 0;
    }

    // Define a url com o since
    var url = 'https://api.github.com/users?since=' + since;

    try {
        // Cria a requisição
        const response = await axios.get(url);
        const result = response.data;

        // Busca o ultimo since para o link da proxima pagina
        const lastSince = result[result.length - 1].id;
        
        // Monta o JSON com o lastSince, firstPage, nextPage e users
        const json = {
                lastSince: lastSince,
                firstPage: req.protocol + '://' + req.get('host') + '/api/users?since=0',
                nextPage: req.protocol + '://' + req.get('host') + '/api/users?since=' + lastSince,
                users: result
            }

        return json;
    } catch (error) {
        return {
            message: 'Erro ao buscar usuários',
            erro: error.message
        }
    }
        
}

// Retorna os detalhes de um determinado usuario
exports.getUserDetails = async(username) => {

    try {
        // URL
        const URL = 'https://api.github.com/users/' + username;

        // Faz a requisição
        const response = await axios.get(URL);
        const data = response.data;

        return data;
    } catch (error) {
        return {
            message: 'Erro ao buscar detalhes do usuário',
            erro: error.message
        }
    }
    
}


// Retorna os repositórios do usuario
exports.getUserRepos = async(username) => {
    
    try {
        // URL
        const URL = 'https://api.github.com/users/' + username + '/repos?per_page=300';

        // Faz a requisição
        const response = await axios.get(URL);
        const data = response.data;

        return data;
    } catch (error) {
        return {
            message: 'erro ao buscar repositórios',
            erro: error.message
        }
    }
    
}
