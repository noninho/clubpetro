// Define a porta padrão do SV
const SV_PORT = 3000;

module.exports = {
    URL_BASE_API: 'http://localhost:' + SV_PORT + '/api',
    SV_PORT: SV_PORT
}