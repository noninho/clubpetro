'use strict'

// Express
const express = require('express');
const app = express();
const handlebars = require('express-handlebars');
const path = require('path');

// Config
    // Seta o handlebars
    app.engine('handlebars', handlebars({defaultLayout: 'indexLayout'}));
    app.set('view engine', 'handlebars');
    // Muda o caminho das views
    app.set('views', path.join(__dirname + '/src/views'));

    // Public
    app.use(express.static(path.join(__dirname, 'src/public')));

    
// Rotas
    // importa as rotas
    const mainRoutes = require('./src/routes/main-routes');
    const apiRoutes = require('./src/routes/api-routes');
    const userRoutes = require('./src/routes/user-routes');
    
    // Rotas principais
    app.use('/', mainRoutes);

    // Rotas da API
    app.use('/api', apiRoutes);

    // Rotas dos usuarios
    app.use('/users', userRoutes);

// Export
module.exports = app